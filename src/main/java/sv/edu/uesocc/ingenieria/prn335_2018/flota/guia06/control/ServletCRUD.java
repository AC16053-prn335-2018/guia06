/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sv.edu.uesocc.ingenieria.prn335_2018.flota.guia06.control;

import java.io.IOException;
import java.io.PrintWriter;
import java.lang.ProcessBuilder.Redirect;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import sv.edu.uesocc.ingenieria.prn335_2018.flota.datos.definicion.TipoUsuario;

/**
 *
 * @author cristian
 */
@WebServlet(name = "ServletCRUD", urlPatterns = {"/ServletCRUD"})
public class ServletCRUD extends HttpServlet {

    @Inject
    public Utilidades utilidades;

    TipoUsuario tipoUsuario = new TipoUsuario();

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<h1>Grupo Jueves</h1>");
            out.println("<h2>AC16053</h2>");
            out.println("<h2>Tipo Usuario</h2>");
            out.println("</head>");
            out.println("<body>");
            String act = request.getParameter("acc");
            if (null == act) {
                out.println("<h2>No se selecionó nada</h2> ");
            } else {
                String st = request.getParameter("txtIdUsuario").trim();
                String nombre = request.getParameter("txtNombre");
                String appId = request.getParameter("txtAppId");
                String activo = request.getParameter("txtActivo");
                String observaciones = request.getParameter("txtObservaciones");
                Integer idUsuario = null;
                if (!(st == null || "".equals(st))) {
                    idUsuario = Integer.parseInt(st);
                }
                tipoUsuario.setIdTipoUsuario(idUsuario);
                tipoUsuario.setNombre(nombre);
                tipoUsuario.setAppId(appId);
                if ("1".equals(activo) || "true".equals(activo)) {
                    tipoUsuario.setActivo(true);
                } else {
                    tipoUsuario.setActivo(false);
                }
                tipoUsuario.setObservaciones(observaciones);
                List<TipoUsuario> Lista = new ArrayList<>();

//---------------------------------------------------------------------------------------------------------------------------------
                switch (act) {
                    case "Eliminar":
                        utilidades.delete(tipoUsuario);
                        if (utilidades.selectAll().isEmpty()) {
                            out.println("<h2>Se encuentra vacia la lista</h2> ");
                        } else {
                            out.println("<table border=\"1\">");
                            out.println(" <thead>");
                            out.println(" <tr>");
                            out.println("<th>ID Tipo Usuario</th>");
                            out.println("<th>Nombre</th>");
                            out.println("<th>App ID</th>");
                            out.println("<th>Activo</th>");
                            out.println("<th>Observaciones</th>");
                            out.println("</tr>");
                            out.println(" </thead>");
                            out.println("  <tbody>");
                            Lista = utilidades.selectAll();
                            for (int i = 0; i < Lista.size(); i++) {
                                out.println("\n");
                                out.println(" <tr>");
                                out.println(" <td>" + Lista.get(i).getIdTipoUsuario() + "</td>");
                                out.println(" <td>" + Lista.get(i).getNombre() + "</td>");
                                out.println(" <td>" + Lista.get(i).getAppId() + "</td>");
                                out.println(" <td>" + Lista.get(i).getActivo() + "</td>");
                                out.println(" <td>" + Lista.get(i).getObservaciones() + "</td>");
                                out.println(" </tr>");
                            }
                            out.println(" </tbody>");
                            out.println("  </table>");
                        }
                        break;
                    case "Actualizar":
                        utilidades.update(tipoUsuario);
                        if (utilidades.selectAll().isEmpty()) {
                            out.println("<h2>Se encuentra vacia la lista</h2> ");
                        } else {
                            out.println("<table border=\"1\">");
                            out.println(" <thead>");
                            out.println(" <tr>");
                            out.println("<th>ID Tipo Usuario</th>");
                            out.println("<th>Nombre</th>");
                            out.println("<th>App ID</th>");
                            out.println("<th>Activo</th>");
                            out.println("<th>Observaciones</th>");
                            out.println("</tr>");
                            out.println(" </thead>");
                            out.println("  <tbody>");
                            Lista = utilidades.selectAll();
                            for (int i = 0; i < Lista.size(); i++) {
                                out.println("\n");
                                out.println(" <tr>");
                                out.println(" <td>" + Lista.get(i).getIdTipoUsuario() + "</td>");
                                out.println(" <td>" + Lista.get(i).getNombre() + "</td>");
                                out.println(" <td>" + Lista.get(i).getAppId() + "</td>");
                                out.println(" <td>" + Lista.get(i).getActivo() + "</td>");
                                out.println(" <td>" + Lista.get(i).getObservaciones() + "</td>");
                                out.println(" </tr>");
                            }
                            out.println(" </tbody>");
                            out.println("  </table>");
                        }
                        break;
                    case "Crear":
                        utilidades.insert(tipoUsuario);
                         if (utilidades.selectAll().isEmpty()) {
                            out.println("<h2>Se encuentra vacia la lista</h2> ");
                        } else {
                            out.println("<table border=\"1\">");
                            out.println(" <thead>");
                            out.println(" <tr>");
                            out.println("<th>ID Tipo Usuario</th>");
                            out.println("<th>Nombre</th>");
                            out.println("<th>App ID</th>");
                            out.println("<th>Activo</th>");
                            out.println("<th>Observaciones</th>");
                            out.println("</tr>");
                            out.println(" </thead>");
                            out.println("  <tbody>");
                            Lista = utilidades.selectAll();
                            for (int i = 0; i < Lista.size(); i++) {
                                out.println("\n");
                                out.println(" <tr>");
                                out.println(" <td>" + Lista.get(i).getIdTipoUsuario() + "</td>");
                                out.println(" <td>" + Lista.get(i).getNombre() + "</td>");
                                out.println(" <td>" + Lista.get(i).getAppId() + "</td>");
                                out.println(" <td>" + Lista.get(i).getActivo() + "</td>");
                                out.println(" <td>" + Lista.get(i).getObservaciones() + "</td>");
                                out.println(" </tr>");
                            }
                            out.println(" </tbody>");
                            out.println("  </table>");
                        }

                        break;
                    case "Mostrar":

                        if (utilidades.selectAll().isEmpty()) {
                            out.println("<h2>Se encuentra vacia la lista</h2> ");
                        } else {
                            out.println("<table border=\"1\">");
                            out.println(" <thead>");
                            out.println(" <tr>");
                            out.println("<th>ID Tipo Usuario</th>");
                            out.println("<th>Nombre</th>");
                            out.println("<th>App ID</th>");
                            out.println("<th>Activo</th>");
                            out.println("<th>Observaciones</th>");
                            out.println("</tr>");
                            out.println(" </thead>");
                            out.println("  <tbody>");
                            Lista = utilidades.selectAll();
                            for (int i = 0; i < Lista.size(); i++) {
                                out.println("\n");
                                out.println(" <tr>");
                                out.println(" <td>" + Lista.get(i).getIdTipoUsuario() + "</td>");
                                out.println(" <td>" + Lista.get(i).getNombre() + "</td>");
                                out.println(" <td>" + Lista.get(i).getAppId() + "</td>");
                                out.println(" <td>" + Lista.get(i).getActivo() + "</td>");
                                out.println(" <td>" + Lista.get(i).getObservaciones() + "</td>");
                                out.println(" </tr>");
                            }
                            out.println(" </tbody>");
                            out.println("  </table>");
                        }
                        break;
                    case "Buscar":
                        out.println("<table border=\"1\">");
                        out.println(" <thead>");
                        out.println(" <tr>");
                        out.println("<th>ID Tipo Usuario</th>");
                        out.println("<th>Nombre</th>");
                        out.println("<th>App ID</th>");
                        out.println("<th>Activo</th>");
                        out.println("<th>Observaciones</th>");
                        out.println("</tr>");
                        out.println(" </thead>");
                        out.println("  <tbody>");
                        int xd = Integer.parseInt(request.getParameter("txtIdBuscarTipoUsuario"));
                        out.println("<h2>ella sabia xdd " + xd + "</h2> ");

                        tipoUsuario.setIdTipoUsuario(xd);
                        Lista = utilidades.findById(xd);
                        out.println(" <tr>");
                        out.println(" <td>" + Lista.get(0).getIdTipoUsuario() + "</td>");
                        out.println(" <td>" + Lista.get(0).getNombre() + "</td>");
                        out.println(" <td>" + Lista.get(0).getAppId() + "</td>");
                        out.println(" <td>" + Lista.get(0).getActivo() + "</td>");
                        out.println(" <td>" + Lista.get(0).getObservaciones() + "</td>");
                        out.println(" </tr>");
                        out.println(" </tbody>");
                        out.println("  </table>");
                        out.println("<a href='index.html'>Volver al index</a>");

                        break;
                    case "BuscarRango":
                        int inicio = Integer.parseInt(request.getParameter("txtRangoInicio"));
                        int fin = Integer.parseInt(request.getParameter("txtRangoFin"));
                        Lista = utilidades.findRange(inicio, fin);
                        if (Lista.isEmpty()) {
                            out.println("<h2>Se encuentra vacia la lista</h2> ");
                        } else {
                            out.println("<table border=\"1\">");
                            out.println(" <thead>");
                            out.println(" <tr>");
                            out.println("<th>ID Tipo Usuario</th>");
                            out.println("<th>Nombre</th>");
                            out.println("<th>App ID</th>");
                            out.println("<th>Activo</th>");
                            out.println("<th>Observaciones</th>");
                            out.println("</tr>");
                            out.println(" </thead>");
                            out.println("  <tbody>");
                            for (int i = 0; i < Lista.size(); i++) {
                                out.println("\n");
                                out.println(" <tr>");
                                out.println(" <td>" + Lista.get(i).getIdTipoUsuario() + "</td>");
                                out.println(" <td>" + Lista.get(i).getNombre() + "</td>");
                                out.println(" <td>" + Lista.get(i).getAppId() + "</td>");
                                out.println(" <td>" + Lista.get(i).getActivo() + "</td>");
                                out.println(" <td>" + Lista.get(i).getObservaciones() + "</td>");
                                out.println(" </tr>");
                            }
                            out.println(" </tbody>");
                            out.println("  </table>");
                        }
                        break;
                    default:
                        break;
                }
            }
            out.println("</body>");
            out.println("</html>");
        } catch (Exception ex) {
            Logger.getLogger(ServletCRUD.class.getName()).log(Level.SEVERE, ex.getMessage(), ex);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
