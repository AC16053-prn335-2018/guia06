package sv.edu.uesocc.ingenieria.prn335_2018.flota.guia06.control;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityNotFoundException;
import javax.persistence.Persistence;
import javax.persistence.Query;
import sv.edu.uesocc.ingenieria.prn335_2018.flota.datos.definicion.TipoUsuario;

/**
 *
 * @author cristian
 */
@Stateless
@LocalBean
public class Utilidades implements Serializable {

    EntityManagerFactory emf = Persistence.createEntityManagerFactory("flota_unit");
    public EntityManager em = emf.createEntityManager();

    public void insert(TipoUsuario tipoUsuario) throws Exception {
        try {
            if (em != null) {
                em.getTransaction().begin(); //Inicio de la transaccion
                em.persist(tipoUsuario); //Inserta los parametros dados
                em.getTransaction().commit();//Fin de la transaccion
            }
        } catch (Exception e) {
            Logger.getLogger(Utilidades.class.getName()).log(Level.SEVERE, e.getMessage(), e);
            em.getTransaction().rollback(); //corta toda la transaccion si ocurre un error y no guarda nada, además si la PK se deja vacia no guarda la transaccion
        }
    }

    public void update(TipoUsuario tipoUsuario) throws Exception {
        try {
            if (em != null) {
                em.getTransaction().begin();//Inicio de la transaccion
                em.merge(tipoUsuario); //actualiza los parametro dado
                em.getTransaction().commit();//Fin de la transaccion
            }
        } catch (EntityNotFoundException notFound) {
            Logger.getLogger(Utilidades.class.getName()).log(Level.SEVERE, notFound.getMessage(), notFound);
            em.getTransaction().rollback(); //corta toda la transaccion si ocurre un error y no guarda nada, además si la PK se deja vacia no guarda la transaccion
        }
    }

    public void delete(TipoUsuario tipoUsuario) throws Exception {
        if (em != null && tipoUsuario != null) {
            try {
                System.out.println("NO SON NULOS");
                em.getTransaction().begin();
                TipoUsuario user = em.find(TipoUsuario.class, tipoUsuario.getIdTipoUsuario());
                em.remove(user);
                em.getTransaction().commit();
            } catch (EntityNotFoundException enfe) {
                Logger.getLogger(Utilidades.class.getName()).log(Level.SEVERE, enfe.getMessage(), enfe);
                em.getTransaction().rollback();
            }
        } else {
            System.out.println("NULOS");
        }
    }

    public List<TipoUsuario> selectAll() throws Exception {

        List<TipoUsuario> Lista = new ArrayList<>();
        try {
            if (em != null) {
                Lista = em.createNamedQuery("TipoUsuario.findAll").getResultList();
            }
        } catch (Exception e) {
            Logger.getLogger(Utilidades.class.getName()).log(Level.SEVERE, e.getMessage(), e);
            em.getTransaction().rollback();
        }
        return Lista;
    }

    public List<TipoUsuario> findById(int IdEntity) {
        List<TipoUsuario> ListaBusqueda = new ArrayList<>(); 
        try {
            if (em != null && IdEntity > 0 && IdEntity != 0) {
                String jpql = "SELECT t FROM TipoUsuario t WHERE t.idTipoUsuario = "+IdEntity;
                Query query = em.createQuery(jpql);
                    ListaBusqueda = query.getResultList(); 
            }
        } catch (Exception e) {
            Logger.getLogger(Utilidades.class.getName()).log(Level.SEVERE, e.getMessage(), e);
            em.getTransaction().rollback(); 
        }
        return ListaBusqueda; 
    }

    public List<TipoUsuario> findRange(int inicio, int totalRegistros) {
        List<TipoUsuario> List = new ArrayList<>();
        try {
            if (em != null) {
                if(inicio<=0 || totalRegistros<=0){
                } else {
                    Query query = em.createQuery("SELECT t FROM TipoUsuario t WHERE t.idTipoUsuario BETWEEN "+inicio+" AND "+totalRegistros);
                    List = query.getResultList();
                    em.close();
                }
            }
        } catch (Exception e) {
            Logger.getLogger(Utilidades.class.getName()).log(Level.SEVERE, e.getMessage(), e);
            em.getTransaction().rollback();
        }
        return List;
    }

}